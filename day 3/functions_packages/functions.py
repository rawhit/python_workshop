# # # # # # ## function programming
# # # # # # #  declaration

# # # # # # def printsomething():
# # # # # # 	print("gap re")
# # # # # # ##calling function
# # # # # # printsomething()
# # # # # # #####3
# # # # # # a=printsomething
# # # # # # a()
# # # # # # print(type(printsomething))
# # # # # # print(type(a))
# # # # # # a()

# # # # # # #######
# # # # # # a=10 #globle variable
# # # # # # b=1 # globle variable
# # # # # # def addIt():
# # # # # # 	print(a+b)

# # # # # # addIt()

# # # # # # def addIt():
# # # # # # 	def addNow():
# # # # # # 		print(a+b) # a n b globle
# # # # # # 	addNow() #calling addNOw
# # # # # # addIt() #calling addIt

# # # # # # ###########3
# # # # # #TYPE 3 FUNCTION WITH PARAMERTER
# # # # # # declaration
# # # a=10
# # # # # b=2

# # # # # def calculator(a,b):
# # # # # 	return(a+b,a-b,a*b,a/b,a%b,a//b,a**b) # pyhton returns multiple things only lang which support 
# # # # # add,sub,mul,div,rem,fdiv,power = calculator(a,b)
# # # # # print(f"add{add}\nsub{sub}")

# # # # # #######33
# # # # # TYPE 4 function with default value
# # # # def divideIt(a=1,b=1):
# # # # 	print(a/b)
# # # # divideIt()
# # # # divideIt(5)
# # # # divideIt(5,10)
# # # # divideIt(b=10,a=122)
# # # # divideIt(10,)
# # # # # divideIt( ,10) #syntax error

# # # ###########3
# # # #TYPE 5 FUNCTION WITH ARGS
# # # def minayArgs(*args):
# # # 	print(args)

# # # def minayArgs(*args):
# # # 	for i in list(args):
# # # 		if (type(i) is type(1234)):
# # # 			print(i)
# # # #calling
# # # minayArgs(1,3,4,"hello",[123,43,"fd"])

# # # def minayArgs(*args):
# # # 	for i in list(args):
# # # 		if (type(i) is type([1,2])):
# # # 			for j in i:
# # # 				print(j)
# # # minayArgs(1,3,4,"hello",[123,43,"fd"])


# # # ###########33


# # # #TYPE ^ FUNCTION with kwargs
# # # #declaration
# # # def hobabakwarg(**kwargs):
# # # 	print(kwargs)

# # # #####calling
# # # hobabakwarg(name="raw",std="SE")


# # # ########3
# # #type 7 function with arg n kwarg
# # #declaration
# # #kwargs #always work for dictionary key n ans value
# # def michroArgsAndKwargs(*args,**kwargs):
# # 	print(args)
# # 	print(kwargs) 
# # michroArgsAndKwargs(1,2,3,"hello",name="raw",std="se")


# ##########33
# #TYPE 8  lambda function
# # ######## single line function

# # inc=lambda x: x+1
# # dec = lambda x: x-1
# # print(inc(35))
# # print(dec(35))
# # great=lambda x,y: x if x>y else y
# # print(great(15,20))

# ######TYPE 9
# #FILTER  filter (func,data)
# # data=list(range(1,11))
# # logic=lambda x: x%2==0
# # print(list(filter(logic,data)))

# #######type 10
# # map map(function,data)
# # data=list(range(1,7))
# # l=lambda x: x*x
# # print(list(map(l,data)))

# # data=["GOA","mahaRashtra","PUNJAB","lucknow"]

# # def checkCase(x):
# # 	if x.islower():
# # 		return x.upper()
# # 	elif x.isupper():
# # 		return x.lower()
# # 	else:
# # 		return x.swapcase()

# # l=lambda x: checkCase(x)
# # print(list(map(l,data)))

# ###############33
# #type 11 function reduce
# # data=list(range(1,10))
# # from functools import reduce  #packge importing imp
# # print(reduce(lambda x,y:x+y,data))

# #############3
# #type 12  ZIP FUNCTION
# # list1=["red","black","orange"]
# # list2=[1,2,3]
# # cd=zip(list1,list2)
# # print(list(cd))
# # list1=["red","black","orange"]
# # list2=[1,2,3]
# # cd=zip(list1,list2)
# # print(tuple(cd))
# # list1=["red","black","orange"]
# # list2=[1,3]
# # cd=zip(list1,list2)
# # print(list(cd))

# # ########type 13 enumerate
# # list1=["red","green","yellow","orange"]
# # for index,data in enumerate(list1):
# # 	print(index,data)

# ###########type 14  closure ,decorator
# # def wrapper(funcn):
# # 	def inner():
# # 		print("before execption")
# # 		funcn()
# # 		print("after execption")
# # 	return inner

# # # def printMe():
# # # 	print("hello")
# # printMe=wrapper(printMe)
# # printMe()
# #wrapper is decortor
# # @wrapper#printMe =wrapper(PrintMe) decorator
# # def printMe():
# # 	print("hello all")
# # # printMe()

# # def checkValue(func):
# # 	def inner(*args,**kwargs):
# # 		if type(args[0])==type("str"):
# # 			print("check u r PARAMERTER")
# # 		else:
# # 			func(*args,**kwargs)
		
# # 	return inner
# # @checkValue
# # def add(a,b):
# # 	print(a+b)
# # add(3,5)
# # add("3","5")

# ##################################












# ############################
# ##base exception is main parent
# # -------------
# def firstFlow():
# 	try:
# 		no1=input("ENTER no1=")
# 		no2=input("ENter no2=")
# 		div = n01/no2
# 	except Exception as e:
# 		print("ERROR OCCUFRED")
# 		alternateFlow(no1,no2)
# 	else:
# 		print(f"division  value is {div}")
# def alternateFlow(no1,no2):
# 	try:
# 		if type(no1)==type("") or type(no2)==type(""):
# 			no1=float(no1)
# 			no2=float(no2)
# 			div=no1/no2
# 		else:
# 			div=no1/no2
# 	except Exception as e:
# 		print("error occured in alternating flow, transferring to ")
# 		firstFlow()
# 	else:
# 		print(f"division value is {div}")
# #firstFlow()
# def customException():
# 	try:
# 		no1=float(input("enter no1"))
# 		no2=float(input("enter no2"))
# 		if (no1/no2)%2==0:
# 			output=(f"division is {no1/no2} and it is not even ")
# 		else:
# 			raise BaseExecption("division output ")
# 	except BaseException as e:
# 		if (input("do u want 2 see error msg(y/n)")=="y"):
# 			print(e)
# 		print("try again!")
# 		customException()
# 	else:
# 		print(output)
# # customException()




# # for sql w3.com
# #################################33
def insertData(username,password):
	try:
		import pymysql as sql
		db=sql.connect("localhost","root","","python_demo")#ip,username 
		cursor=db.cursor()
		sql=f"insert into user (username,password) values('{username}','{password}')"
		cursor.execute(sql)
		db.commit()
		if(cursor.rowcount>0):
			output="data inserted succesfully"
		else:
			output="data not inserted"
	except Exception as e:
		db.rollback()
		print(e)
	else:
		print(output)
username=input("enter username")
password=input("enter password")
# insertData(username,password)

def getAllData():
	try:
		import pymysql as sql
		db=sql.connect("localhost","root","","python_demo")#ip,username 
		cursor=db.cursor()
		sql="select * from user"
		cursor.execute(sql)
		rs=cursor.fetchall()
	except Exception as e:
		raise e
	else:
		print(rs)
getAllData()
		

























#  