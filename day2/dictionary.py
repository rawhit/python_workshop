# dict1 = {
# 	"name":"lokesh",
# 	"std":"BE",
# 	"roll_np":"36",
# 	"courses":["java","python"],
# 	"address":{"landmark":"lalbaug","pincode":"400033"},

# }
#print(dict1)
# ##############33333
# RULES _________----------is mutable
# 1.key-immutable 
# values-could be mutable or immutable
# 2. if same key assigned then last assignmt always eins
# 3.dictonary doesnt follows index follow
# 4.dictionary follow, key value


##read
#print(dict1["std"])

##update note dic is mutable therfore update n del is possible
# dict1["name"]="raj"
# print(dict1)

####del
#del dict1["name"]
#print(dict1)


#####
#function method
# clear()
# copy()
# fromkeys()
# ____contains___()
# #has_key()

# get()
# setdefault()

# #update
# keys()
# values()
# items()
# dict1.clear()
# print(dict1)

# ##########333
dict1 = {"name":"lokesh",
        "std":"be","roll":36}
dict2 = dict1
dict3 = dict1.copy()
print(dict1)
print(dict2)
print(dict3)

####


# dict1["name"]="rajesh"
# print(dict1)
# print(dict2)
# print(dict3) #
##usage of copy function
           ###it will change only when data is mutable


    ############
#############
list1= ["name","std","roll","address"]
print({}.fromkeys(list1,"NA"))
##########
print(dict1.__contains__("name"))
print(dict1.get("name"))

print(dict1.setdefault("address"))
print(dict1)
print(dict1.setdefault("name"))
print(dict1)


###########3
dict2 = {"contact":"8765456789","address":"lalbaug"}
dict1.update(dict2)
print(dict1)
###########
print(list(dict1.keys()))
print(list(dict1.values()))
print(list(dict1.items()))
